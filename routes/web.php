<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', action: [HomeController::class, 'index']);
// Route::get('/register', [AuthController::class, 'register'])->name('register');
// Route::post('/welcome', [AuthController::class, 'welcome'])->name('welcome');


Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/welcome', 'AuthController@welcome')->name('welcome');
// Route::get('/register', function () {
//     return view('form');
// });

// Route::get('/register', function () {
//     return view('register');
// });
